import openai
openai.api_key = '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'
def obtener_respuesta(texto_entrada, rol):
    texto_completo = f"Rol: {rol}\nUsuario: {texto_entrada}"
    respuesta = openai.Completion.create(
        engine="text-davinci-003",
        prompt=texto_completo,
        max_tokens=200,
        temperature=0.5,
        stop=None,
    )

    respuesta_generada = respuesta.choices[0].text.strip()
    tokens_utilizados = respuesta.usage.total_tokens

    return respuesta_generada, tokens_utilizados