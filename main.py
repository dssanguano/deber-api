import uvicorn as uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from RespuestaToken import obtener_respuesta

app = FastAPI()


class Entrada(BaseModel):
    texto: str
    rol: str

@app.post('/respuesta')
def obtener_respuesta_endpoint(entrada: Entrada):
    respuesta_generada, tokens_utilizados = obtener_respuesta(entrada.texto, entrada.rol)
    return {"respuesta": respuesta_generada, "tokens_utilizados": tokens_utilizados}

if __name__ == '__main__':
    uvicorn.run(app, port=8000)
